package com.example.mauro.security;

import android.Manifest;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class AlarmPageSet extends AppCompatActivity {

    private final String phoneNumber1 = "3407173317";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_page_set);

    }

    //Send Message to Disarm Alarm

    public void SendMessageDisarm(View view){
        //simple version

        try {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.SEND_SMS},1);

            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage( phoneNumber1, null, "Bus will be at your door in 30 seconds", null, null);
            displayExceptionMessage("SMS Sent");
            Log.d("---", "Sent");

        }
        catch (Exception e) {
            displayExceptionMessage("SMS faild, please try again");
            Log.d("---","Fail");
            e.printStackTrace();
        }
    }

    //Send Message to Active Full Alarm
    public void SenMessageActiveFull(View view){

        try {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.SEND_SMS},1);

            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage( phoneNumber1, null, "Bus will be at your door in 30 seconds", null, null);
            displayExceptionMessage("SMS Sent");
            Log.d("---", "Sent");

        }
        catch (Exception e) {
            displayExceptionMessage("SMS faild, please try again");
            Log.d("---","Fail");
            e.printStackTrace();
        }

    }

    //Send Message to Active Parz Full Alarm
    public void SendMessageActiveParz(View view){

        try {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.SEND_SMS},1);

            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage( phoneNumber1, null, "Bus will be at your door in 30 seconds", null, null);
            displayExceptionMessage("SMS Sent");
            Log.d("---", "Sent");

        }
        catch (Exception e) {
            displayExceptionMessage("SMS faild, please try again");
            Log.d("---","Fail");
            e.printStackTrace();
        }

    }

    //View Toast Exception
    public void displayExceptionMessage(String msg)
    {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

}



