package com.example.mauro.security;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import static android.provider.AlarmClock.EXTRA_MESSAGE;


public class MainActivity extends AppCompatActivity {

    public static String phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Create Listener
        EditText Field1 = (EditText)findViewById(R.id.editText_phoneNumber);
        //tmp.addTextChangedListener(readPhoneNumber());

        //Create Listener
        Field1.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

                if(s.length() != 0)
                    phoneNumber = s.toString();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count){
            }
        });
    }


    //Event to Start Button
    public void StartAppMessage(View view){

        if(phoneNumber == null || phoneNumber.length() <= 0){
            //View Toast Message
            Toast.makeText(this, "Not Insert Telephone Number!",
                    Toast.LENGTH_LONG).show();
        }
        else{
            Intent intent = new Intent(this, AlarmPageSet.class);   //call alarmPageSet
            EditText editText = (EditText) findViewById(R.id.editText_phoneNumber); //Passage Value
            String message = editText.getText().toString();
            intent.putExtra(EXTRA_MESSAGE, message);
            startActivity(intent);
        }
    }

    //Control value of telephone
    public void readPhoneNumber(){
        EditText editText = (EditText) findViewById(R.id.editText_phoneNumber); //Passage Value
        String tmp = editText.getText().toString();

        if(tmp.length() > 0){
            //correct
            phoneNumber = tmp;
        }
        else{
            //View Toast Message
            Toast.makeText(this, "Error Telephone Number!",
                    Toast.LENGTH_LONG).show();
        }

    }


}
